#!/usr/bin/env iocsh.bash
# -----------------------------------------------------------------------------
# ESI Pressure Gauge startup cmd
# -----------------------------------------------------------------------------
require(esipressuregauge)

# -----------------------------------------------------------------------------
# Environment variables
# -----------------------------------------------------------------------------
epicsEnvSet("IPADDR",   "192.168.1.32")
epicsEnvSet("IPPORT",   "4001")
epicsEnvSet("SYS",      "SES-DAC-01:")
epicsEnvSet("DEV",      "Pmonit-PGxx-001")
epicsEnvSet("PREFIX",   "$(SYS)$(DEV)")
epicsEnvSet("LOCATION", "$(SYS) $(IPADDR)")
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(esiPressureGauge_DIR)db")

# -----------------------------------------------------------------------------
# loading databases
# -----------------------------------------------------------------------------
# E3 Common databases
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

# ESI Pressure Gauge
iocshLoad("$(esipressuregauge_DIR)esiPressureGauge.iocsh", "PREFIX=$(PREFIX), IPADDR=$(IPADDR), IPPORT=$(IPPORT)")
